#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif


FOUNDATION_EXPORT double Pods_Soda365Gps_Soda365GpsTestsVersionNumber;
FOUNDATION_EXPORT const unsigned char Pods_Soda365Gps_Soda365GpsTestsVersionString[];


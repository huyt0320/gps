export const icons = {
  back: require("./arrow-left.png"),
  bullet: require("./bullet.png"),
}
export interface IconTypes {
  type: "AntDesign" | "Entypo" | "EvilIcons" | "Feather" | "FontAwesome" | "FontAwesome5" | "Fontisto" | "Foundation" | "Ionicons" | "MaterialCommunityIcons" | "CustomIcon",
  name: string
}
// export type IconTypes = keyof typeof icons

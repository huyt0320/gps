import React from "react"
import { View, ViewStyle, TextStyle } from "react-native"
import { HeaderProps } from "./header.props"
import { Button } from "../button/button"
import { Text } from "../text/text"
import { Icon } from "../icon/icon"
import { spacing } from "../../theme"
import { translate } from "../../i18n/"
import { useSafeArea } from "react-native-safe-area-context"
import { VectorIcon } from ".."

// static styles
const ROOT: ViewStyle = {
  flexDirection: "row",
  paddingHorizontal: spacing[4],
  alignItems: "center",
  paddingTop: spacing[5],
  paddingBottom: spacing[5],
  backgroundColor: "#2A6FA8",
  justifyContent: "flex-start",
}
const TXT_IC: TextStyle = {
  color: "#FFF",
  fontSize: 20,
  width: 32,
}
const TITLE: TextStyle = { textAlign: "center", fontSize: 18 }
const TITLE_MIDDLE: ViewStyle = { flex: 1, justifyContent: "center" }
const LEFT: ViewStyle = { width: 32 }
const RIGHT: ViewStyle = { width: 32 }

/**
 * Header that appears on many screens. Will hold navigation buttons and screen title.
 */
export function Header(props: HeaderProps) {
  const {
    onLeftPress,
    onRightPress,
    rightIcon,
    leftIcon,
    headerText,
    headerTx,
    style,
    titleStyle,
  } = props
  const header = headerText || (headerTx && translate(headerTx)) || ""
  const { top } = useSafeArea()
  return (
    <View style={{ ...ROOT, ...style, paddingTop: top + spacing[2] }}>
      {leftIcon ? (
        <Button preset="link" onPress={onLeftPress}>
          <VectorIcon type={leftIcon.type} name={leftIcon.name} style={TXT_IC} />
        </Button>
      ) : (
        <View style={LEFT} />
      )}
      <View style={TITLE_MIDDLE}>
        <Text style={{ ...TITLE, ...titleStyle }} text={header} />
      </View>
      {rightIcon ? (
        <Button preset="link" onPress={onRightPress}>
          <VectorIcon type={rightIcon.type} name={rightIcon.name} style={TXT_IC} />
        </Button>
      ) : (
        <View style={RIGHT} />
      )}
    </View>
  )
}

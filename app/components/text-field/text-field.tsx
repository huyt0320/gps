import React from "react"
import { View, TextInput, TextStyle, ViewStyle } from "react-native"
import { color, spacing, typography } from "../../theme"
import { translate } from "../../i18n"
import { Text } from "../text/text"
import { TextFieldProps } from "./text-field.props"
import { mergeAll, flatten } from "ramda"
import { VectorIcon } from "../vector-icon/vector-icon"
import { TouchableOpacity } from "react-native-gesture-handler"

// the base styling for the container
const CONTAINER: ViewStyle = {
  paddingVertical: spacing[1],
  flexDirection: "row",
  alignItems: "center",
  paddingHorizontal: 10,
  backgroundColor: "#fff",
  // width: "100%"
}

// the base styling for the TextInput
const INPUT: TextStyle = {
  fontFamily: typography.primary,
  color: color.text,
  fontSize: 18,
  paddingVertical: 5,
  flex: 1,
  // width: "100%",
  backgroundColor: color.palette.white,
}
const LABEL: TextStyle = {
  fontSize: 14,
  color: "#666E81",
  paddingBottom: 5,
}
const ROOT: ViewStyle = {
  paddingTop: 5,
  width: "100%",
  // flex: 1
}

const ICONINPUT: ViewStyle = {
  backgroundColor: "white",
  marginLeft: 5,
  paddingVertical: 5,
  // paddingHorizontal: 15,
  borderRadius: 8,
}

// currently we have no presets, but that changes quickly when you build your app.
const PRESETS: { [name: string]: ViewStyle } = {
  default: {},
}

const enhance = (style, styleOverride) => {
  return mergeAll(flatten([style, styleOverride]))
}

/**
 * A component which has a label and an input together.
 */
export function TextField(props: TextFieldProps) {
  const {
    placeholderTx,
    placeholder,
    labelTx,
    label,
    preset = "default",
    style: styleOverride,
    inputStyle: inputStyleOverride,
    forwardedRef,
    isLeftIcon,
    isRightIcon,
    name,
    name2,
    color1,
    color2,
    isLabel,
    viewStyle,
    labelStyle,
    onPress,
    isRequired,
    onPress2,
    styleInputIcon,
    ...rest
  } = props
  let containerStyle: ViewStyle = { ...CONTAINER, ...PRESETS[preset] }
  containerStyle = enhance(containerStyle, styleOverride)
  let inputStyle: TextStyle = INPUT
  inputStyle = enhance(inputStyle, inputStyleOverride)
  const actualPlaceholder = placeholderTx ? translate(placeholderTx) : placeholder

  return (
    <TouchableOpacity activeOpacity={0.9} onPress={onPress}>
      <View style={[ROOT, { ...viewStyle }]}>
        {isLabel && (
          <View style={{ flexDirection: "row" }}>
            <Text
              preset="fieldLabel"
              style={[LABEL, { ...labelStyle }]}
              tx={labelTx}
              text={label}
            />
            {isRequired && <Text style={{ color: "red" }} text={" (*)"} />}
          </View>
        )}
        <View style={containerStyle}>
          {isLeftIcon && (
            <VectorIcon type={"Feather"} name={name} size={20} color={color1 || "#798093"} />
          )}
          <TextInput
            placeholder={actualPlaceholder}
            placeholderTextColor={color.palette.lighterGrey}
            underlineColorAndroid={color.transparent}
            {...rest}
            style={inputStyle}
            ref={forwardedRef}
          />
          {isRightIcon && (
            <TouchableOpacity
              activeOpacity={1}
              style={[ICONINPUT, { ...styleInputIcon }]}
              onPress={onPress2}
            >
              <VectorIcon type={"Feather"} name={name2} size={25} color={color2 || "#798093"} />
            </TouchableOpacity>
          )}
        </View>
      </View>
    </TouchableOpacity>
  )
}

import { TextInputProps, TextStyle, ViewStyle } from "react-native"

export interface TextFieldProps extends TextInputProps {
  /**
   * The placeholder i18n key.
   */
  placeholderTx?: string

  /**
   * The Placeholder text if no placeholderTx is provided.
   */
  placeholder?: string

  /**
   * The label i18n key.
   */
  labelTx?: string

  /**
   * The label text if no labelTx is provided.
   */
  label?: string

  /**
   * Optional container style overrides useful for margins & padding.
   */
  style?: ViewStyle | ViewStyle[]

  /**
   * Optional style overrides for the input.
   */
  inputStyle?: TextStyle | TextStyle[]

  /**
   * Various look & feels.
   */
  preset?: "default"

  forwardedRef?: any
  isRightIcon?: boolean
  isLeftIcon?: boolean
  name?: string
  name2?: string
  color1?: string
  color2?: string
  isLabel?: boolean
  viewStyle?: ViewStyle | ViewStyle[]
  labelStyle?: TextStyle
  onPress?: () => void
  isRequired?: boolean
  onPress2?: () => void
  styleInputIcon?: ViewStyle
}

import * as React from "react"
import { TextStyle, View, ViewStyle, StyleSheet } from "react-native"
import { color, typography } from "../../theme"
import { Text } from "../"
import AntDesign from "react-native-vector-icons/AntDesign"
import Entypo from "react-native-vector-icons/Entypo"
import EvilIcons from "react-native-vector-icons/EvilIcons"
import Feather from "react-native-vector-icons/Feather"
import FontAwesome from "react-native-vector-icons/FontAwesome"
import FontAwesome5 from "react-native-vector-icons/FontAwesome5"
import Fontisto from "react-native-vector-icons/Fontisto"
import Foundation from "react-native-vector-icons/Foundation"
import Ionicons from "react-native-vector-icons/Ionicons"
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import { VectorIconProps } from "./vector-icon.props"
/**
 * Describe your component here
 */

export function VectorIcon(props: VectorIconProps) {
  const { style, type, name, color, size } = props
  const iconStyle = { ...style }
  switch (type) {
    case "AntDesign":
      return <AntDesign name={name} style={iconStyle} color={color} size={size} />
    case "Entypo":
      return <Entypo name={name} style={iconStyle} color={color} size={size} />
    case "EvilIcons":
      return <EvilIcons name={name} style={iconStyle} color={color} size={size} />
    case "Feather":
      return <Feather name={name} style={iconStyle} color={color} size={size} />
    case "FontAwesome":
      return <FontAwesome name={name} style={iconStyle} color={color} size={size} />
    case "FontAwesome5":
      return <FontAwesome5 name={name} style={iconStyle} color={color} size={size} />
    case "Fontisto":
      return <Fontisto name={name} style={iconStyle} color={color} size={size} />
    case "Foundation":
      return <Foundation name={name} style={iconStyle} color={color} size={size} />
    case "Ionicons":
      return <Ionicons name={name} style={iconStyle} color={color} size={size} />
    case "MaterialCommunityIcons":
      return <MaterialCommunityIcons name={name} style={iconStyle} color={color} size={size} />
  }
}

import { DURATION } from 'rn-simple-toast'
let toastRef

type ToastType = "danger" | "success" | "warning"

const utils = {
  setToast: (_toast) => {
    toastRef = _toast
  },
  showToast: (message, type: ToastType = 'danger') => {
    let color = '#e74c3c'
    switch (type) {
      case 'success':
        color = '#32B85D'
        break
      case 'warning':
        color = '#ff9900'
        break
    }
    toastRef && toastRef.show(message, color, DURATION.LONG)
  },
  sum(obj) {
    return Object.keys(obj).reduce((sum, key) => sum + parseFloat(obj[key] || 0), 0)
  },
  convertLength: (donVi) => {
    return donVi / 100
  },
  wait(timeout) {
    return new Promise((resolve) => {
      setTimeout(resolve, timeout)
    })
  },
}

export default utils

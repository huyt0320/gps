import { Dimensions, Platform, PixelRatio } from 'react-native'

export const configs = {
  windowWidth: Dimensions.get('window').width,
  windowHeight: Dimensions.get('window').height
}

const { width, height } = Dimensions.get('window')

// Size Iphone 11 pro
const guidelineBaseWidth = 375
const guidelineBaseHeight = 812

const scale = size => (width / guidelineBaseWidth) * size
const verticalScale = size => (height / guidelineBaseHeight) * size
const moderateScale = (size, factor = 0.5) => {
  return size + ((verticalScale(size) - size) * factor)
}

export { scale, verticalScale, moderateScale }

export default configs

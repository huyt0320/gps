import React, { useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { TextStyle, View, ViewStyle, FlatList, TouchableOpacity } from "react-native"
import { Button, Header, Screen, Text, TextField, VectorIcon } from "../../components"
import { useNavigation } from "@react-navigation/native"
import Modal from "react-native-modal"
import { scale, verticalScale } from "../../theme/dimension"
import { db } from "../../services/config"
import { color } from "../../theme"
import utils from "../../utils/utils"

const ROOT: ViewStyle = {
  flex: 1,
  backgroundColor: "#DEDADA",
}
const TEXTFIELD: ViewStyle = {
  borderWidth: 1,
  borderColor: "#dbdbdb",
  borderRadius: 8,
  marginVertical: verticalScale(5),
}
const VIEWMODAL: ViewStyle = {
  backgroundColor: "white",
  borderRadius: 8,
  paddingVertical: verticalScale(8),
  paddingHorizontal: scale(10),
}
const BUTTONSAVE: ViewStyle = {
  backgroundColor: "#2A6FA8",
  paddingVertical: scale(15),
  marginTop: scale(15),
}
const TEXTADD: TextStyle = {
  color: "black",
  fontSize: 20,
  textAlign: "center",
  marginBottom: scale(10),
}
const TEXTSAVE: TextStyle = {
  fontSize: 15,
}
const VIEWITEM: ViewStyle = {
  backgroundColor: "white",
  marginHorizontal: scale(10),
  marginVertical: verticalScale(5),
  borderRadius: 4,
  flex: 1,
  paddingHorizontal: scale(10),
  paddingVertical: verticalScale(5),
  flexDirection: "row",
  alignItems: "center",
}
const TEXTCONTENT: TextStyle = { color: "black", fontSize: 20, color: "#172C50" }

export const AccountManagerScreen = observer(function AccountManagerScreen() {
  const [isModal, setisModal] = useState(false)
  const [listAccout, setlistAccout] = useState([])
  const [user, setUser] = useState("")
  const navigation = useNavigation()
  const [isEdit, setisEdit] = useState(false)
  const [password, setpassword] = useState("")
  const [objectC, setobjectC] = useState({})

  const getList = () => {
    // db.ref("/accounts").on("value", (querySnapShot) => {
    //   console.log("dataaaa", querySnapShot.getKey())
    //   const data = querySnapShot.val() ? querySnapShot.val() : {}
    //   // const todoItems = { ...data }
    //   const todoItems = Object.values(data)
    //   setlistAccout(todoItems)
    // })
    db.ref("/accounts").once("value", (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        let obj = {}
        // let objToAdd1 = childSnapshot.val()
        // let objToAdd2 = childSnapshot.key

        // obj variabile could be empty or not, it's the same
        obj = { ...obj, ...childSnapshot.key }
        // obj = { ...obj, ...objToAdd2 }
        // a = { ...objectC, ...childSnapshot.val(), ...childSnapshot.key }
        // setobjectC(childSnapshot.val())
        // objectC.id = childSnapshot.key
        listAccout.push(obj)
      })
    })
  }

  useEffect(() => {
    getList()
  }, [])

  function addNewTodo() {
    setisModal(false)
    db.ref("/accounts").push({
      name: user,
      passwords: password,
    })
    getList()
    utils.showToast("Thêm tài khoản thành công", "success")
  }

  function updateUser() {
    setisModal(false)
    db.ref("/accounts/-MceMHWxVVPvouGSr1S_").update({
      name: user,
      passwords: password,
    })
  }

  return (
    <View style={ROOT}>
      <Header
        leftIcon={{
          type: "Ionicons",
          name: "arrow-back",
        }}
        onLeftPress={() => navigation.goBack()}
        headerText={"Quản lý tài khoản"}
        rightIcon={{
          type: "Ionicons",
          name: "person-add",
        }}
        onRightPress={() => {
          setisModal(!isModal)
          setUser("")
          setpassword("")
          setisEdit(false)
        }}
      />
      {console.log("listAccout", listAccout)}
      <FlatList
        data={listAccout}
        renderItem={({ item, index }) => {
          return (
            <TouchableOpacity
              activeOpacity={1}
              onPress={() => {
                setisEdit(true)
                setisModal(!isModal)
                setUser(item.name)
                setpassword(item.password)
              }}
              style={VIEWITEM}
            >
              <VectorIcon
                size={80}
                color={"#D5E9FA"}
                name={"person-circle-outline"}
                type={"Ionicons"}
              />
              <Text style={TEXTCONTENT} text={item.name} />
            </TouchableOpacity>
          )
        }}
      />
      <Modal onBackdropPress={() => setisModal(false)} isVisible={isModal}>
        <View style={VIEWMODAL}>
          <Text text={isEdit ? "Cập nhật tài khoản" : "Thêm tài khoản"} style={TEXTADD} />
          <TextField
            style={TEXTFIELD}
            inputStyle={{ color: color.storybookDarkBg }}
            onChangeText={(text) => setUser(text)}
            placeholder={"Tên đăng nhập"}
            value={user}
          />
          <TextField
            style={TEXTFIELD}
            inputStyle={{ color: color.storybookDarkBg }}
            onChangeText={(text) => setpassword(text)}
            placeholder={"Mật khẩu"}
            secureTextEntry={true}
            value={password}
          />
          <Button
            textStyle={TEXTSAVE}
            onPress={() => (isEdit === false ? addNewTodo() : updateUser())}
            text={isEdit ? "Cập nhật" : "Lưu"}
            style={BUTTONSAVE}
          />
        </View>
      </Modal>
    </View>
  )
})

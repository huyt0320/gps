import React, { useState } from "react"
import { observer } from "mobx-react-lite"
import { Image, StatusBar, TextStyle, View, ViewStyle } from "react-native"
import { Button, TextField } from "../../components"
import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { scale, verticalScale } from "../../theme/dimension"
import { color } from "../../theme"
import utils from "../../utils/utils"

const ROOT: ViewStyle = {
  flex: 1,
  backgroundColor: "white",
  alignItems: "center",
  // justifyContent: "center",
}
const VIEWINPUT: ViewStyle = {
  marginTop: 30,
  width: "100%",
  paddingVertical: 10,
  paddingHorizontal: 20,
  justifyContent: "center",
  alignItems: "center",
}
const STYLEINPUT: ViewStyle = {
  borderRadius: 4,
  paddingVertical: 10,
  marginVertical: 5,
  backgroundColor: "#ededed",
}
const INPUT: TextStyle = {
  width: "95%",
  flex: 0,
  paddingHorizontal: 10,
  color: "black",
  backgroundColor: "#ededed",
}
const BUTTON: ViewStyle = {
  backgroundColor: color.blue,
  width: "100%",
  paddingVertical: 15,
  borderRadius: 8,
  marginTop: 20,
}
const TEXTBUTTON: TextStyle = { color: "#fff", fontSize: 15, fontWeight: "700" }
export const LoginScreen = observer(function LoginScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // OR
  // const rootStore = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()

  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")

  return (
    <View style={ROOT}>
      <StatusBar backgroundColor={"white"} barStyle={"dark-content"} />
      <Image
        style={{ width: scale(200), height: verticalScale(150), marginTop: verticalScale(100) }}
        resizeMode={"contain"}
        source={require("./logo.jpg")}
      />
      <View style={VIEWINPUT}>
        <TextField
          isLeftIcon
          name={"mail"}
          onChangeText={(text) => setEmail(text)}
          color1={color.blue}
          // style={INPUT}
          value={email}
          style={STYLEINPUT}
          inputStyle={INPUT}
          placeholder={"Tài khoản"}
        />
        <TextField
          isLeftIcon
          name={"lock"}
          color1={color.blue}
          onChangeText={(text) => setPassword(text)}
          value={password}
          secureTextEntry
          style={STYLEINPUT}
          inputStyle={INPUT}
          placeholder={"Mật khẩu"}
        />
        <Button
          onPress={() => {
            navigation.navigate("home")
            // if (email === "823501278345" && password === "32677219") {
            //   setTimeout(() => {
            //     navigation.navigate("home")
            //   }, 500)
            // } else if(email === "" || password === "") {
            //   utils.showToast('Vui lòng điền tài khoản để đăng nhập')
            // } else {
            //   utils.showToast('Vui lòng điền đúng thông tin tài khoản')
            // }
          }}
          style={BUTTON}
          textStyle={TEXTBUTTON}
          text={"Đăng nhập"}
        />
      </View>
    </View>
  )
})

import React, { useEffect } from "react"
import { observer } from "mobx-react-lite"
import {
  FlatList,
  Image,
  StatusBar,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
  PermissionsAndroid,
  Alert,
} from "react-native"
import { Screen, Text, VectorIcon } from "../../components"
import { useNavigation } from "@react-navigation/native"
import { color } from "../../theme"
import { scale, verticalScale } from "../../theme/dimension"
import utils from "../../utils/utils"
import { ic_zalo } from "../../theme/images"

const ROOT: ViewStyle = {
  flex: 1,
}

const VIEWITEM: ViewStyle = {
  flex: 1,
  justifyContent: "center",
  alignItems: "center",
  marginVertical: verticalScale(10),
}

const ICON: ViewStyle = {
  width: scale(80),
  height: verticalScale(80),
  borderRadius: 100,
  justifyContent: "center",
  alignItems: "center",
}

const CONTENT: TextStyle = {
  color: "black",
  fontSize: 15,
  textAlign: "center",
  marginTop: verticalScale(5),
}

const data = [
  { id: 1, name: "Vị trí định vị", color: "#e78377", icon: "crosshair", type: "Feather" },
  {
    id: 2,
    name: "Lịch sử vị trí",
    color: "#f7cf78",
    icon: "map-clock-outline",
    type: "MaterialCommunityIcons",
  },
  {
    id: 3,
    name: "Cuộc gọi thoại",
    color: "#7ed4f7",
    icon: "phone-in-talk",
    type: "MaterialCommunityIcons",
  },
  {
    id: 4,
    name: "Tin nhắn thường",
    color: "#63bbcf",
    icon: "message-text",
    type: "MaterialCommunityIcons",
  },
  { id: 5, name: "Ghi âm", color: "#68baf6", icon: "microphone-alt", type: "FontAwesome5" },
  {
    id: 6,
    name: "Đổi số điện thoại",
    color: "#7ebb53",
    icon: "card-account-phone",
    type: "MaterialCommunityIcons",
  },
  {
    id: 7,
    name: "Thêm số điện thoại",
    color: "#a5c858",
    icon: "phone-plus",
    type: "MaterialCommunityIcons",
  },
  {
    id: 8,
    name: "Messenger",
    color: "#6ba7dd",
    icon: "facebook-messenger",
    type: "MaterialCommunityIcons",
  },
  {
    id: 9,
    name: "Zalo",
    color: "#0280C7",
    icon: "facebook-messenger",
    type: "MaterialCommunityIcons",
  },
  {
    id: 10,
    name: "Video",
    color: "#b700ea",
    icon: "television-play",
    type: "MaterialCommunityIcons",
  },
  {
    id: 11,
    name: "Hình ảnh",
    color: "#ff5454",
    icon: "ios-images",
    type: "Ionicons",
  },
  {
    id: 12,
    name: "Quản lý tài khoản",
    color: "#dc655f",
    icon: "settings",
    type: "Feather",
  },
  {
    id: 13,
    name: "Đăng xuất",
    color: "#dc655f",
    icon: "log-out",
    type: "Feather",
  },
]

export const HomeScreen = observer(function HomeScreen() {
  const navigation = useNavigation()

  useEffect(() => {
    requestLocationPermission()
  }, [])

  async function requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: "Example App",
          message: "Example App access to your location ",
        },
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the location")
      } else {
        console.log("location permission denied")
      }
    } catch (err) {
      console.warn(err)
    }
  }

  const _onPress = ({ item, index }) => {
    switch (index) {
      case 0:
        navigation.navigate("liveTracking")
        break
      case 1:
        utils.showToast("Phần mềm đang cập nhật")
        break
      case 2:
        utils.showToast("Phần mềm đang cập nhật")
        break
      case 3:
        utils.showToast("Phần mềm đang cập nhật")
        break
      case 4:
        utils.showToast("Phần mềm đang cập nhật")
        break
      case 5:
        utils.showToast("Phần mềm đang cập nhật")
        break
      case 6:
        utils.showToast("Phần mềm đang cập nhật")
        break
      case 7:
        utils.showToast("Phần mềm đang cập nhật")
        break
      case 8:
        utils.showToast("Phần mềm đang cập nhật")
        break
      case 9:
        utils.showToast("Phần mềm đang cập nhật")
        break
      case 10:
        utils.showToast("Phần mềm đang cập nhật")
        break
      case 11:
        navigation.navigate("accountManager")
        break
      case 12:
        Alert.alert("Thông báo", "Bạn có muốn đăng xuất ?", [
          {
            text: "Huỷ bỏ",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel",
          },
          { text: "Đồng ý", onPress: () => navigation.navigate("login") },
        ])
        break
      default:
        break
    }
  }

  return (
    <View style={ROOT}>
      <StatusBar backgroundColor={"green"} barStyle={"light-content"} />
      <View style={{ backgroundColor: "green", width: "100%", height: verticalScale(150) }}></View>
      <FlatList
        style={{ backgroundColor: color.background, flex: 1 }}
        data={data}
        contentContainerStyle={{ margin: 4 }}
        numColumns={3}
        horizontal={false}
        renderItem={({ item, index }) => (
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => _onPress({ item, index })}
            style={VIEWITEM}
          >
            <View style={[ICON, { backgroundColor: item.color }]}>
              {item.id !== 9 ? (
                <VectorIcon
                  type={item.type}
                  name={item.type !== null && item.icon}
                  size={30}
                  color={"white"}
                />
              ) : (
                <Image
                  source={ic_zalo}
                  resizeMode={"contain"}
                  style={{ width: scale(35), height: verticalScale(35) }}
                />
              )}
            </View>
            <Text style={CONTENT} text={item.name} />
          </TouchableOpacity>
        )}
      />
    </View>
  )
})

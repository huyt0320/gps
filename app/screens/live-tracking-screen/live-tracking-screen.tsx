import React, { useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { StatusBar, StyleSheet, View, ViewStyle, TouchableOpacity } from "react-native"
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps"
import { VectorIcon } from "../../components"
import { verticalScale } from "../../theme/dimension"
import { useNavigation } from "@react-navigation/native"
import Geolocation from "react-native-geolocation-service"

const ROOT: ViewStyle = {
  ...StyleSheet.absoluteFillObject,
  flex: 1,
  justifyContent: "flex-end",
  alignItems: "center",
}
const MAP: ViewStyle = {
  ...StyleSheet.absoluteFillObject,
}
const BACK: ViewStyle = {
  position: "absolute",
  top: 50,
  left: 30,
  bottom: 0,
}

export const LiveTrackingScreen = observer(function LiveTrackingScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // OR
  // const rootStore = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()
  const [latCurr, setlatCurr] = useState(0)
  const [longCurr, setlongCurr] = useState(0)

  useEffect(() => {
    Geolocation.getCurrentPosition((position) => {
      let { latitude, longitude } = position.coords
      setlatCurr(latitude)
      setlongCurr(longitude)
    })
  }, [])

  return (
    <View style={ROOT}>
      <StatusBar backgroundColor={"white"} barStyle={"dark-content"} />
      <MapView
        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
        style={MAP}
        showsUserLocation
        region={{
          latitude: latCurr,
          longitude: longCurr,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}
      >
        <Marker
          coordinate={{
            latitude: latCurr || 0,
            longitude: longCurr || 0,
          }}
        >
          <VectorIcon type={"Ionicons"} name="location" size={40} color={"red"} />
        </Marker>
      </MapView>
      <TouchableOpacity activeOpacity={1} onPress={() => navigation.goBack()} style={BACK}>
        <VectorIcon type={"AntDesign"} name={"arrowleft"} size={25} color={"black"} />
      </TouchableOpacity>
    </View>
  )
})
